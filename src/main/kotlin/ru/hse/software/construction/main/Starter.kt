package ru.hse.software.construction.main

import ru.hse.software.construction.controllers.FilmController
import ru.hse.software.construction.controllers.InputController
import ru.hse.software.construction.controllers.SessionController
import ru.hse.software.construction.controllers.TicketController
import ru.hse.software.construction.exceptions.BackException
import ru.hse.software.construction.exceptions.ExitException
import ru.hse.software.construction.menu.MessageSpawner
import ru.hse.software.construction.repository.FilmRepository
import ru.hse.software.construction.repository.ScheduleRepository
import ru.hse.software.construction.*
import ru.hse.software.construction.services.*

import ru.hse.software.construction.users.UsersRepository
import ru.hse.software.construction.menu.*

class Starter {
    private fun run(menu: Menu, packageRepository: PackageRepository, inputController: InputController) {
        menu.printMenu(packageRepository.getAuthOptions())
        try {
            inputController.handle(packageRepository.getAuthOptions())
        } catch (e: Exception) {
            if (e.message == "Exit") {
                return;
            }
        }
        while (true) {
            menu.printMenu(packageRepository.getMainMenuOptions())
            try {
                inputController.handle(packageRepository.getMainMenuOptions())
            } catch (b: BackException) {
                continue
            } catch (e: ExitException) {
                return;
            } catch (e: Exception) {
                println("Incorrect input. Try again.")
            }
        }
    }

    fun start() {
        val deserializer: Deserializer = Deserializer()
        val messageSpawner: MessageSpawner = MessageSpawner()
        val serializer: Serializer = Serializer()
        val filmRepository: FilmRepository = FilmRepository()
        filmRepository.init()
        val filmService: FilmService = FilmService(filmRepository, serializer, messageSpawner)
        val userRepository: UsersRepository = UsersRepository()
        userRepository.init()
        val scheduleRepository: ScheduleRepository = ScheduleRepository(deserializer, filmService)
        scheduleRepository.init()
        val scheduleService: ScheduleService = ScheduleService(scheduleRepository, serializer, messageSpawner)
        val menu: Menu = Menu(messageSpawner)
        val packageRepository: PackageRepository = PackageRepository()
        val sessionController: SessionController = SessionController(filmService, scheduleService)
        val filmController: FilmController = FilmController(filmService)
        val ticketController: TicketController = TicketController(scheduleService)
        val inputServicesRepository: InputServicesRepository =
            InputServicesRepository(
                userRepository,
                menu,
                packageRepository,
                filmService,
                scheduleService,
                sessionController,
                filmController,
                ticketController
            )
        val inputController: InputController = InputController(inputServicesRepository)
        inputServicesRepository.bindInputController(inputController)
        inputServicesRepository.initServices()
        run(menu, packageRepository, inputController)
    }

}
