package ru.hse.software.construction.controllers

import ru.hse.software.construction.services.FilmService

class FilmController(
    private var filmService: FilmService,

    ) {
    fun getFilmName(): String {
        var filmName: String
        while (true) {
            println("Input film name")
            filmName = readln()
            when (filmService.checkIn(filmName)) {
                true -> break
                false -> println("Film does not exist")
            }
        }
        return filmName
    }

    fun getFilmNameAdd(): String {
        var filmName: String
        while (true) {
            println("Input film name")
            filmName = readln()
            when (!filmService.checkIn(filmName)) {
                true -> break
                false -> println("Film does not exist")
            }
        }
        return filmName
    }
}