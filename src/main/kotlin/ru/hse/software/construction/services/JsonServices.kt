package ru.hse.software.construction.services

import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import ru.hse.software.construction.models.Schedule
import ru.hse.software.construction.repository.FilmRepository
import ru.hse.software.construction.users.User
import ru.hse.software.construction.users.UsersRepository
import java.io.File

class Deserializer {

    fun usersDeserialize(): MutableList<User> {
        val text = File("users.json").readText()
        return when (text == "") {
            true -> mutableListOf<User>()
            false -> {
                val usersRepository = Json.decodeFromString<UsersRepository>(text)
                usersRepository.getUsers()
            }
        }
    }

    fun scheduleDeserialize(): Schedule {
        val text = File("schedule.json").readText()
        return when (text == "") {
            true -> Schedule()
            false -> {
                Json.decodeFromString<Schedule>(text)
            }
        }
    }

    fun filmsDeserialize(): FilmRepository {
        val text = File("films.json").readText()
        return when (text == "") {
            true -> FilmRepository()
            false -> {
                Json.decodeFromString<FilmRepository>(text)

            }
        }
    }


}

class Serializer {

    fun serializeUserRepository(path: String, repository: UsersRepository) {
        val json = Json.encodeToString(repository)
        File(path).writeText(json)
    }

    fun serializeSchedule(path: String, repository: Schedule) {
        val json = Json.encodeToString(repository)
        File(path).writeText(json)
    }

    fun serializeFilmRepository(path: String, repository: FilmRepository) {
        val json = Json.encodeToString<FilmRepository>(repository)
        File(path).writeText(json)
    }
}