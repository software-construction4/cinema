package ru.hse.software.construction.controllers

import ru.hse.software.construction.exceptions.BackException
import ru.hse.software.construction.exceptions.ExitException
import ru.hse.software.construction.menu.TypeChoose
import ru.hse.software.construction.menu.Package
import ru.hse.software.construction.services.InputServicesRepository

class InputController(
    private val inputServices: InputServicesRepository
) {

    fun handle(pack: Package) {
        var input: String
        var numberOfChoose: Int
        var typeChoose: TypeChoose
        while (true) {
            input = readln()
            try {
                numberOfChoose = input.toInt()
                typeChoose = pack.getPackageOptions()[numberOfChoose - 1].typeChoose
                when (typeChoose) {
                    TypeChoose.REGISTER -> inputServices.getService(TypeChoose.REGISTER).startOption()
                    TypeChoose.LOGIN -> inputServices.getService(TypeChoose.LOGIN).startOption()
                    TypeChoose.EXIT -> throw ExitException("Exit")
                    TypeChoose.EDIT_FILMS -> inputServices.getService(TypeChoose.EDIT_FILMS).startOption()
                    TypeChoose.ADD_FILM -> inputServices.getService(TypeChoose.ADD_FILM).startOption()
                    TypeChoose.DELETE_FILM -> inputServices.getService(TypeChoose.DELETE_FILM).startOption()
                    TypeChoose.UPDATE_FILM -> inputServices.getService(TypeChoose.UPDATE_FILM).startOption()
                    TypeChoose.VIEW_FILMS -> inputServices.getService(TypeChoose.VIEW_FILMS).startOption()
                    TypeChoose.VIEW_SCHEDULE -> inputServices.getService(TypeChoose.VIEW_SCHEDULE).startOption()
                    TypeChoose.EDIT_SESSIONS -> inputServices.getService(TypeChoose.EDIT_SESSIONS).startOption()
                    TypeChoose.ADD_SESSION -> inputServices.getService(TypeChoose.ADD_SESSION).startOption()
                    TypeChoose.UPDATE_SESSION -> inputServices.getService(TypeChoose.UPDATE_SESSION).startOption()
                    TypeChoose.DELETE_SESSION -> inputServices.getService(TypeChoose.DELETE_SESSION).startOption()
                    TypeChoose.BUY_TICKET -> inputServices.getService(TypeChoose.BUY_TICKET).startOption()
                    TypeChoose.VIEW_AUDITORIUM -> inputServices.getService(TypeChoose.VIEW_AUDITORIUM).startOption()
                    TypeChoose.REFUND_TICKET -> inputServices.getService(TypeChoose.REFUND_TICKET).startOption()
                    TypeChoose.TAKE_SEAT -> inputServices.getService(TypeChoose.TAKE_SEAT).startOption()
                    TypeChoose.BACK -> throw BackException("Back")
                    else -> TODO()
                }
                return
            } catch (e: ExitException) {
                throw e
            } catch (e: BackException) {
                throw e
            } catch (e: Exception) {
                println("Incorrect input. Try again.")
            }
        }
    }
}



