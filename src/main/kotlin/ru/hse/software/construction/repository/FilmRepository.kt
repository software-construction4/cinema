package ru.hse.software.construction.repository

import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient
import ru.hse.software.construction.models.Film
import ru.hse.software.construction.services.Deserializer
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths

@Serializable
class FilmRepository(
    @Transient
    private var deserializer: Deserializer = Deserializer()
) {
    private var films: MutableList<Film> = mutableListOf()
    fun init() {
        when (Files.exists(Paths.get("films.json"))) {
            true -> {
                val filmRepository = deserializer.filmsDeserialize()
                films = filmRepository.films
            }

            false -> File("films.json").createNewFile()
        }
    }

    fun getAllFilms(): MutableList<Film> {
        return films
    }

    fun addFilm(film: Film) {
        films.add(film)
    }

    fun deleteFilm(filmName: String) {
        films.removeIf { it.name == filmName }
    }
}