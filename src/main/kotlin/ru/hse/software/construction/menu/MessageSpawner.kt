package ru.hse.software.construction.menu

import ru.hse.software.construction.models.Film
import ru.hse.software.construction.models.Session

class MessageSpawner {
    fun spawn(pack: Package) {
        pack.getPackageOptions().forEachIndexed() { index, option ->
            println("${index + 1}) ${option.message}")
        }
    }

    fun printFilms(films: MutableList<Film>) {
        films.forEach {
            println(it)
        }
    }

    fun printSchedule(sessions: List<Session>) {
        sessions.forEach {
            println(it)
        }
    }
}