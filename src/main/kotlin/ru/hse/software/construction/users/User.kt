package ru.hse.software.construction.users

import kotlinx.serialization.Serializable

@Serializable
class User(
    val login: String,
    val password: String
) {

}