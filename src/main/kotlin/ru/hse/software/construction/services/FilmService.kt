package ru.hse.software.construction.services

import ru.hse.software.construction.menu.MessageSpawner
import ru.hse.software.construction.models.Film
import ru.hse.software.construction.repository.FilmRepository

class FilmService(
    private val filmRepository: FilmRepository,
    private val serializer: Serializer,
    private val messageSpawner: MessageSpawner
) {
    fun save() {
        serializer.serializeFilmRepository("films.json", filmRepository)
    }

    fun checkIn(filmName: String): Boolean {
        return filmRepository.getAllFilms().any { it.name == filmName }
    }

    fun addFilm(film: Film) {
        filmRepository.addFilm(film)
        save()
    }

    fun getFilm(filmName: String): Film {
        return filmRepository.getAllFilms().first { it.name == filmName }
    }

    fun printFilm(filmName: String) {
        val film = getFilm(filmName)
        println(film)
    }

    fun printFilms() {
        messageSpawner.printFilms(filmRepository.getAllFilms())
    }

    fun deleteFilm(filmName: String) {
        filmRepository.deleteFilm(filmName)
        save()
    }

}