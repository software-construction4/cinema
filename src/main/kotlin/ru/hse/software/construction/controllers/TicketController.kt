package ru.hse.software.construction.controllers

import kotlinx.datetime.toKotlinLocalDateTime
import ru.hse.software.construction.models.Seat
import ru.hse.software.construction.models.Session
import ru.hse.software.construction.models.Ticket
import ru.hse.software.construction.services.ScheduleService
import java.time.LocalDateTime

class TicketController(
    private val scheduleService: ScheduleService,
) {
    fun getSeat(session: Session): Pair<Int, Int> {
        println(session.auditorium)
        while (true) {
            try {
                println("Input row number in format:  row number")
                val input = readln().split(" ")
                val row = input[0].toInt()
                val number = input[1].toInt()
                if (row > 10 || row < 0 || number > 10 || number < 0) {
                    println("A non-existent number or row of a place.Choose correct seat")
                    continue
                }
                when (scheduleService.seatIsEmpty(row, number, session)) {
                    true -> return Pair(row, number)
                    false -> println("Seat is not empty. Choose another one.")
                }
            } catch (e: Exception) {
                println("Incorrect input. Try again.")
            }
        }
    }

    fun getSeatToRefund(session: Session): Pair<Int, Int> {
        println(session.auditorium)
        while (true) {
            try {
                println("Input row number in format: row number")
                val input = readln().split(" ")
                val row = input[0].toInt()
                val number = input[1].toInt()
                if (row > 10 || row < 0 || number < 0 || number > 10) {
                    println("A non-existent number or row of a place.Choose correct seat")
                    continue
                }
                return Pair(row, number)
            } catch (e: Exception) {
                println("Incorrect input. Try again.")
            }
        }
    }

    private fun getBuyerName(): String {
        println("Input buyer's name")
        return readln()
    }

    fun getTicket(session: Session): Ticket {
        val rowNumber = getSeat(session)
        val row = rowNumber.first
        val number = rowNumber.second
        val buyerName = getBuyerName()
        return Ticket(Seat(false, row, number), buyerName, LocalDateTime.now().toKotlinLocalDateTime())
    }

}