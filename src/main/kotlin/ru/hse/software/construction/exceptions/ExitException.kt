package ru.hse.software.construction.exceptions

class ExitException(
    message: String
) : Exception(message)