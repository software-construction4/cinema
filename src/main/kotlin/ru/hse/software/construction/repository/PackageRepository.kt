package ru.hse.software.construction.repository


import ru.hse.software.construction.menu.*

class PackageRepository {
    private var packages: MutableList<Package> = mutableListOf<Package>(
        PackageAuthOptions(),
        PackageMainMenuOptions(),
        PackageEditFilmOptions(),
        PackageEditSessionOptions()
    )

    fun getAuthOptions(): Package {
        return packages[0];
    }

    fun getMainMenuOptions(): Package {
        return packages[1];
    }

    fun getEditFilmOptions(): Package {
        return packages[2];
    }

    fun getEditSessionOptions(): Package {
        return packages[3];
    }
}