package ru.hse.software.construction.repository

import kotlinx.datetime.LocalDateTime
import ru.hse.software.construction.models.Schedule
import ru.hse.software.construction.models.Session
import ru.hse.software.construction.services.Deserializer
import ru.hse.software.construction.services.FilmService
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths

class ScheduleRepository(
    private var deserializer: Deserializer = Deserializer(),
    private var filmService: FilmService
) {
    private var schedule: Schedule = Schedule()
    fun init() {
        when (Files.exists(Paths.get("schedule.json"))) {
            true -> {
                schedule = deserializer.scheduleDeserialize()
                linkToFilms()
            }

            false -> File("schedule.json").createNewFile()
        }
    }

    fun getSchedule(): Schedule {
        return schedule
    }

    fun addSession(session: Session) {
        schedule.sessions.add(session)
    }

    private fun linkToFilms() {
        schedule.sessions.forEach {
            it.film = filmService.getFilm(it.film.name)
        }
    }

    fun getSession(filmName: String, startTime: LocalDateTime): Session {
        return schedule.sessions.first { it.film.name == filmName && it.startTimeFilm == startTime }
    }

    fun deleteSession(session: Session) {
        schedule.sessions.remove(session)
    }
}