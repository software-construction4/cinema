package ru.hse.software.construction.models

import kotlinx.serialization.Serializable


@Serializable
class Schedule {
    var sessions: MutableList<Session> = mutableListOf()
}
