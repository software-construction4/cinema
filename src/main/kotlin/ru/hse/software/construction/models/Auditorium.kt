package ru.hse.software.construction.models

import kotlinx.serialization.Serializable

@Serializable
class Auditorium(
    private val numberRows: Int,
    private val numberColumns: Int
) {
    var seats = Array(numberRows) { row ->
        Array(numberColumns) { numberColumns ->
            Seat(
                true,
                row + 1,
                numberColumns + 1,
            )
        }
    }

    override fun toString(): String {
        val greenColor = "\u001b[32m"
        val red = "\u001b[31m"
        val reset = "\u001b[0m"
        val resultString = StringBuilder()
        resultString.append(numberRows)

        for (row in seats.withIndex()) {
            resultString.append("\nRow ${row.index + 1}\t")
            for (seat in row.value.withIndex()) {
                if (seat.value.isEmpty) {
                    resultString.append(greenColor)
                    resultString.append("[${seat.index + 1}]\t")
                    resultString.append(reset)
                } else {
                    resultString.append(red)
                    resultString.append("[${seat.index + 1}]\t")
                    resultString.append(reset)
                }
            }
            resultString.append("\n")
        }

        return resultString.toString()
    }
}