package ru.hse.software.construction.services

import java.security.Key
import javax.crypto.Cipher
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.DESKeySpec
import kotlin.io.encoding.Base64
import kotlin.io.encoding.ExperimentalEncodingApi

class Cryptographer {
    private val secreteKey: String = "8khi)wok"

    @OptIn(ExperimentalEncodingApi::class)
    fun encrypt(input: String): String {

        val c = Cipher.getInstance("DES")
        val kf = SecretKeyFactory.getInstance("DES")
        val keySpec = DESKeySpec(secreteKey.toByteArray())
        val key: Key? = kf.generateSecret(keySpec)
        c.init(Cipher.ENCRYPT_MODE, key)
        val encrypt = c.doFinal(input.toByteArray())
        return Base64.encode(encrypt)
    }
}