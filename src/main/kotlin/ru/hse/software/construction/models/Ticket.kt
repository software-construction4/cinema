package ru.hse.software.construction.models


import kotlinx.serialization.Serializable
import kotlinx.datetime.LocalDateTime

@Serializable
class Ticket(
    var seat: Seat,
    var customerName: String,
    var creationDate: LocalDateTime,
) {
}




