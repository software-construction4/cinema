package ru.hse.software.construction.menu



class Menu(
    private val messageSpawner: MessageSpawner
) {
    fun printMenu(pack: Package) {
        messageSpawner.spawn(pack)
    }
}