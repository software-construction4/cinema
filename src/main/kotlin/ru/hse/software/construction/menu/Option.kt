package ru.hse.software.construction.menu

class PackageRepository {
    private var packages: MutableList<Package> = mutableListOf(
        PackageAuthOptions(),
        PackageMainMenuOptions(),
        PackageEditFilmOptions(),
        PackageEditSessionOptions()
    )

    fun getAuthOptions(): Package {
        return packages[0]
    }

    fun getMainMenuOptions(): Package {
        return packages[1]
    }

    fun getEditFilmOptions(): Package {
        return packages[2]
    }

    fun getEditSessionOptions(): Package {
        return packages[3]
    }
}


class Option(
    val typeChoose: TypeChoose,
    val message: String
)