package ru.hse.software.construction.services

import kotlinx.datetime.LocalDateTime
import kotlinx.datetime.toJavaLocalDateTime
import ru.hse.software.construction.menu.MessageSpawner
import ru.hse.software.construction.models.Session
import ru.hse.software.construction.models.Ticket
import ru.hse.software.construction.repository.ScheduleRepository


class ScheduleService(
    private val scheduleRepository: ScheduleRepository,
    private val serializer: Serializer,
    private val messageSpawner: MessageSpawner
) {
    fun save() {
        serializer.serializeSchedule("schedule.json", scheduleRepository.getSchedule())
    }

    fun addSession(session: Session) {
        scheduleRepository.addSession(session)
        save()
    }

    fun isContainFilm(filmName: String): Boolean {
        return scheduleRepository.getSchedule().sessions.any { it.film.name == filmName }
    }

    private fun overlaps(session1: Session, session2: Session): Boolean {
        val end1 = session1.startTimeFilm.toJavaLocalDateTime().plusMinutes(session1.film.duration.inWholeMinutes)
        val end2 = session2.startTimeFilm.toJavaLocalDateTime().plusMinutes(session2.film.duration.inWholeMinutes)
        if (session1.startTimeFilm == session2.startTimeFilm || end1 == end2) {
            return true
        }
        return session1.startTimeFilm > session2.startTimeFilm && session1.startTimeFilm.toJavaLocalDateTime() <= end2 ||
                session2.startTimeFilm > session1.startTimeFilm && session2.startTimeFilm.toJavaLocalDateTime() <= end1
    }

    fun overlapsSession(session: Session): Boolean {
        for(session1 in scheduleRepository.getSchedule().sessions){
            if(session1!=session && overlaps(session1,session)){
                return true
            }
        }
        return false
    }
    fun overlapsSession(session: Session, sessionPrevious: Session): Boolean {
        for(session1 in scheduleRepository.getSchedule().sessions){
            if(session1!=sessionPrevious && overlaps(session1,session)){
                return true
            }
        }
        return false
    }

    fun printSchedule() {
        if(scheduleRepository.getSchedule().sessions.isEmpty()) {
            println("No sessions")
            return
        }
        messageSpawner.printSchedule(scheduleRepository.getSchedule().sessions.sortedBy { it.startTimeFilm })
    }

    fun containSession(filmName: String, startTime: LocalDateTime): Boolean {
        return scheduleRepository.getSchedule().sessions.any { it.film.name == filmName && it.startTimeFilm == startTime }
    }

    fun getSession(filmName: String, startTime: LocalDateTime): Session {
        return scheduleRepository.getSession(filmName, startTime)
    }

    fun deleteSession(session: Session) {
        scheduleRepository.deleteSession(session)
        save()
    }

    fun seatIsEmpty(row: Int, number: Int, session: Session): Boolean {
        return session.auditorium.seats[row - 1][number - 1].isEmpty
    }

    fun takeSeat(row: Int, number: Int, session: Session) {
        session.auditorium.seats[row - 1][number - 1].isEmpty = false
    }

    fun addTicket(ticket: Ticket, session: Session) {
        session.tickets.add(ticket)
        save()
    }

    fun returnSeat(row: Int, number: Int, session: Session) {
        session.auditorium.seats[row - 1][number - 1].isEmpty = true
    }

    fun removeTicket(ticket: Ticket, session: Session) {
        session.tickets.remove(ticket)
        save()
    }
}
