package ru.hse.software.construction.main

fun main(args: Array<String>) {
    val starter = Starter()
    starter.start()
}
