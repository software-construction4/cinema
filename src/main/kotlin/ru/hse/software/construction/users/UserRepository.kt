package ru.hse.software.construction.users

import java.io.File
import java.nio.file.Files
import java.nio.file.Paths
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient

import ru.hse.software.construction.services.Deserializer
import ru.hse.software.construction.services.Serializer

@Serializable
class UsersRepository(
    private var users: MutableList<User> = mutableListOf(),
    @Transient
    private var deserializer: Deserializer = Deserializer(),
    @Transient
    private var serializer: Serializer = Serializer()
) {
    fun init() {
        when (Files.exists(Paths.get("users.json"))) {
            true -> users = deserializer.usersDeserialize()
            false -> File("users.json").createNewFile()
        }
    }

    fun getUsers(): MutableList<User> {
        return users
    }

    fun checkContainsLogin(login: String): Boolean {
        return users.any { it.login == login }
    }

    fun addUser(user: User) {
        users.add(user)
    }

    fun saveRepository() {
        serializer.serializeUserRepository("users.json", this)
    }
}