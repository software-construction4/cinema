package ru.hse.software.construction.menu

abstract class Package(
    protected var options: MutableList<Option> = mutableListOf()
) {
    fun getPackageOptions(): MutableList<Option> {
        return options
    }
}

class PackageAuthOptions : Package() {

    init {
        options.add(Option(TypeChoose.LOGIN, "Log In"))
        options.add(Option(TypeChoose.REGISTER, "Sign Up"))
        options.add(Option(TypeChoose.EXIT, "Exit"))
    }
}

class PackageMainMenuOptions : Package() {
    init {
        options.add(Option(TypeChoose.EDIT_FILMS, "Edit Film"))
        options.add(Option(TypeChoose.EDIT_SESSIONS, "Edit Session"))
        options.add(Option(TypeChoose.BUY_TICKET, "Buy Ticket"))
        options.add(Option(TypeChoose.REFUND_TICKET, "Refund Ticket"))
        options.add(Option(TypeChoose.TAKE_SEAT, "Take a seat"))
        options.add(Option(TypeChoose.VIEW_FILMS, "View Films"))
        options.add(Option(TypeChoose.VIEW_SCHEDULE, "View Schedule"))
        options.add(Option(TypeChoose.VIEW_AUDITORIUM, "View Auditorium"))
        options.add(Option(TypeChoose.EXIT, "Exit"))
    }
}

class PackageEditFilmOptions : Package() {
    init {
        options.add(Option(TypeChoose.ADD_FILM, "Add Film"))
        options.add(Option(TypeChoose.UPDATE_FILM, "Update Film"))
        options.add(Option(TypeChoose.DELETE_FILM, "Delete Film"))
        options.add(Option(TypeChoose.BACK, "Back"))
        options.add(Option(TypeChoose.EXIT, "Exit"))
    }
}

class PackageEditSessionOptions : Package() {
    init {
        options.add(Option(TypeChoose.ADD_SESSION, "Add Session"))
        options.add(Option(TypeChoose.UPDATE_SESSION, "Update Session"))
        options.add(Option(TypeChoose.DELETE_SESSION, "Delete session"))
        options.add(Option(TypeChoose.BACK, "Back"))
        options.add(Option(TypeChoose.EXIT, "Exit"))
    }
}