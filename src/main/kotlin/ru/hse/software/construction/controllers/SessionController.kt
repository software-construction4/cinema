package ru.hse.software.construction.controllers

import kotlinx.datetime.LocalDateTime
import kotlinx.datetime.toKotlinLocalDateTime
import ru.hse.software.construction.models.Session
import ru.hse.software.construction.services.FilmService
import ru.hse.software.construction.services.ScheduleService
import java.time.format.DateTimeFormatter

class SessionController(
    private var filmService: FilmService,
    private var scheduleService: ScheduleService

) {
    fun getSession(): Session {
        var filmName: String
        while (true) {
            println("Input film name")
            filmName = readln()
            when (filmService.checkIn(filmName)) {
                true -> break
                false -> println("Film does not exist")
            }
        }
        while (true) {
            println("Input start time in format yyyy-MM-dd HH:mm")
            try {
                val firstApiFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")
                val date = java.time.LocalDateTime.parse(readln(), firstApiFormat)
                val startFilm = date.toKotlinLocalDateTime()
                when (scheduleService.containSession(filmName, startFilm)) {
                    true -> return scheduleService.getSession(filmName, startFilm)
                    false -> {
                        println("Session does not exist")
                        continue
                    }

                }

            } catch (e: Exception) {
                println("Invalid duration")
                continue
            }
        }
    }

    fun updateStartTime(session: Session) {
        var startTime: LocalDateTime
        while (true) {
            println("Input new start time in format yyyy-MM-dd HH:mm")
            try {
                val format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")
                val date = java.time.LocalDateTime.parse(readln(), format)
                startTime = date.toKotlinLocalDateTime()
                if (scheduleService.overlapsSession(
                        Session(
                            session.film,
                            session.auditorium,
                            session.tickets,
                            startTime
                        ), session
                    )
                ) {
                    println("Session overlaps")
                    continue
                }
                session.startTimeFilm = startTime
                println("Successfully updated")
                break
            } catch (e: Exception) {
                println("Invalid duration")
                continue
            }
        }
    }

}