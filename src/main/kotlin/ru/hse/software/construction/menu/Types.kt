package ru.hse.software.construction.menu


enum class TypeChoose {
    EDIT_FILMS,
    EDIT_SESSIONS,
    ADD_FILM,
    DELETE_FILM,
    VIEW_FILMS,
    UPDATE_FILM,
    BUY_TICKET,
    REFUND_TICKET,
    VIEW_AUDITORIUM,
    VIEW_SCHEDULE,
    ADD_SESSION,
    DELETE_SESSION,
    UPDATE_SESSION,
    TAKE_SEAT,
    EXIT,
    REGISTER,
    LOGIN,
    BACK,
}