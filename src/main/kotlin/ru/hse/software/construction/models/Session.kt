package ru.hse.software.construction.models

import kotlinx.datetime.LocalDateTime
import kotlinx.datetime.toJavaLocalDateTime
import kotlinx.serialization.Serializable
import java.time.format.DateTimeFormatter
import java.util.*

@Serializable
class Session(
    var film: Film,
    val auditorium: Auditorium,
    var tickets: MutableList<Ticket>,
    var startTimeFilm: LocalDateTime,
) {
    override fun toString(): String {
        val dTF = DateTimeFormatter.ofPattern("dd MMM uuuu HH:mm", Locale.US)
        return "Session:\n Film: ${film.name}\n Beginning: ${dTF.format(startTimeFilm.toJavaLocalDateTime())}\n Duration: ${film.duration}\n"
    }
}