package ru.hse.software.construction.services

import ru.hse.software.construction.controllers.FilmController
import ru.hse.software.construction.controllers.InputController
import ru.hse.software.construction.controllers.SessionController
import ru.hse.software.construction.controllers.TicketController
import kotlinx.datetime.toJavaLocalDateTime
import kotlinx.datetime.toKotlinLocalDateTime
import ru.hse.software.construction.menu.Menu
import ru.hse.software.construction.menu.TypeChoose
import ru.hse.software.construction.users.User
import ru.hse.software.construction.users.UsersRepository
import ru.hse.software.construction.menu.PackageRepository
import ru.hse.software.construction.models.Auditorium
import ru.hse.software.construction.models.Film
import ru.hse.software.construction.models.Session
import ru.hse.software.construction.models.Ticket
import java.time.format.DateTimeFormatter
import kotlin.time.Duration

class InputServicesRepository(
    private var usersRepository: UsersRepository,
    private var menu: Menu,
    private var packageRepository: PackageRepository,
    private var filmService: FilmService,
    private var scheduleService: ScheduleService,
    private var sessionController: SessionController,
    private var filmController: FilmController,
    private var ticketController: TicketController,


    ) {
    private var inputController: InputController? = null
    private var services: MutableMap<TypeChoose, InputService> = mutableMapOf()
    fun initServices() {
        services = mutableMapOf(
            TypeChoose.REGISTER to RegisterService(usersRepository),
            TypeChoose.LOGIN to LogInService(usersRepository),
            TypeChoose.EDIT_FILMS to EditFilmService(menu, packageRepository, inputController),
            TypeChoose.ADD_FILM to AddFilmService(filmService),
            TypeChoose.EDIT_SESSIONS to EditSessionsService(menu, packageRepository, inputController),
            TypeChoose.ADD_SESSION to AddSessionService(scheduleService, filmService, filmController),
            TypeChoose.UPDATE_FILM to UpdateFilmService(filmService, filmController),
            TypeChoose.DELETE_FILM to DeleteFilmService(filmService, scheduleService, filmController),
            TypeChoose.VIEW_FILMS to ViewFilmsService(filmService),
            TypeChoose.VIEW_SCHEDULE to ViewScheduleService(scheduleService),
            TypeChoose.UPDATE_SESSION to UpdateSessionService(scheduleService, sessionController),
            TypeChoose.DELETE_SESSION to DeleteSessionService(scheduleService, sessionController),
            TypeChoose.BUY_TICKET to BuyTicketService(scheduleService, sessionController, ticketController),
            TypeChoose.VIEW_AUDITORIUM to ViewAuditoriumService(sessionController),
            TypeChoose.REFUND_TICKET to RefundTicketService(scheduleService, sessionController, ticketController),
            TypeChoose.TAKE_SEAT to TakeSeatService(scheduleService, sessionController, ticketController)
        )
    }

    fun bindInputController(inputController: InputController) {
        this.inputController = inputController
    }

    fun getService(typeChoose: TypeChoose): InputService {
        return services[typeChoose]!!
    }
}

abstract class InputService {
    abstract fun startOption()
}

class RegisterService(
    private val userRepository: UsersRepository,
    private val cryptographer: Cryptographer = Cryptographer(),
) : InputService(

) {
    override fun startOption() {
        var login: String
        while (true) {
            println("Input Login")
            login = readln()
            when (userRepository.checkContainsLogin(login)) {
                true -> println("Login already exists")
                false -> break
            }
        }
        println("Input Password")
        val password = readln()
        val enc = cryptographer.encrypt(password)
        userRepository.addUser(User(login, enc))
        userRepository.saveRepository()
        println("Successfully registered")
    }
}

class LogInService(
    private val userRepository: UsersRepository,
    private val cryptographer: Cryptographer = Cryptographer(),
) : InputService(

) {
    override fun startOption() {
        var login: String
        var password: String
        while (true) {
            println("Input Login")
            login = readln()
            when (userRepository.checkContainsLogin(login)) {
                true -> break
                false -> println("Login does not exist")
            }
        }
        while (true) {
            println("Input password")
            password = readln()
            when (cryptographer.encrypt(password) == userRepository.getUsers().first { it.login == login }.password) {
                true -> {
                    println("Successfully logged in")
                    break
                }

                false -> println("Invalid password")
            }
        }
    }
}

class EditFilmService(
    private val menu: Menu,
    private val packageRepository: PackageRepository,
    private val inputController: InputController?,
) : InputService(

) {
    override fun startOption() {
        menu.printMenu(packageRepository.getEditFilmOptions())
        inputController!!.handle(packageRepository.getEditFilmOptions())

    }
}

class EditSessionsService(
    private val menu: Menu,
    private val packageRepository: PackageRepository,
    private val inputController: InputController?,
) : InputService(

) {
    override fun startOption() {
        menu.printMenu(packageRepository.getEditSessionOptions())
        inputController!!.handle(packageRepository.getEditSessionOptions())
    }
}

class AddFilmService(
    private var filmService: FilmService
) : InputService(

) {
    override fun startOption() {
        var filmName: String
        var duration: Duration
        while (true) {
            println("Input film name")
            filmName = readln()
            when (!filmService.checkIn(filmName)) {
                true -> break
                false -> println("Film already exist")
            }
        }
        println("Input description")
        val description = readln()
        while (true) {
            println("Input duration in format _h _m")
            try {
                duration = Duration.parse(readln())
                val film = Film(filmName, description, duration)
                filmService.addFilm(film)
                println("Successfully added")
                break
            } catch (e: Exception) {
                println("Invalid duration")
                continue
            }
        }
    }
}

class AddSessionService(
    private var scheduleService: ScheduleService,
    private var filmService: FilmService,
    private var filmController: FilmController,
) : InputService(

) {
    override fun startOption() {
        val filmName: String = filmController.getFilmName()
        val film: Film = filmService.getFilm(filmName)
        val auditorium = Auditorium(10, 10)
        val tickets: MutableList<Ticket> = mutableListOf()
        while (true) {
            println("Input start time in format yyyy-MM-dd HH:mm")
            try {
                val firstApiFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")
                val date = java.time.LocalDateTime.parse(readln(), firstApiFormat)
                val startFilm = date.toKotlinLocalDateTime()
                if (scheduleService.overlapsSession(Session(film, auditorium, tickets, startFilm))) {
                    println("Session overlaps")
                    continue
                }
                scheduleService.addSession(Session(film, auditorium, tickets, startFilm))
                println("Successfully added")
                break
            } catch (e: Exception) {
                println("Invalid duration")
                continue
            }
        }
    }
}

class UpdateFilmService(
    private var filmService: FilmService,
    private var filmController: FilmController
) : InputService(

) {


    private fun updateDescription(film: Film) {
        println("Input new description")
        film.description = readln()
        println("Successfully updated")

    }

    private fun updateDuration(film: Film) {
        while (true) {
            println("Input new duration in format _h _m")
            try {
                film.duration = Duration.parse(readln())
                println("Successfully updated")
                break
            } catch (e: Exception) {
                println("Invalid duration")
                continue
            }
        }
    }

    private fun updateComponent(film: Film) {
        println("Do you want to update description?(y/n)")
        var answer: String = readln()
        if (answer == "y") {
            updateDescription(film)
        }
        println("Do you want to update duration?(y/n)")
        answer = readln()
        if (answer == "y") {
            updateDuration(film)
        }

    }


    override fun startOption() {
        val filmName: String = filmController.getFilmName()
        filmService.printFilm(filmName)
        val film: Film = filmService.getFilm(filmName)
        updateComponent(film)
        filmService.save()
    }
}

class DeleteFilmService(
    private var filmService: FilmService,
    private var scheduleService: ScheduleService,
    private var filmController: FilmController
) : InputService(

) {

    override fun startOption() {
        val filmName: String = filmController.getFilmName()
        filmService.printFilm(filmName)
        when (scheduleService.isContainFilm(filmName)) {
            true -> println("The film is linked to the session, cannot be deleted")
            false -> {
                filmService.deleteFilm(filmName)
                println("Successfully deleted")
            }
        }
    }
}

class ViewFilmsService(
    private val filmService: FilmService
) : InputService() {
    override fun startOption() {
        filmService.printFilms()
    }
}

class ViewScheduleService(
    private val scheduleService: ScheduleService
) : InputService() {
    override fun startOption() {
        scheduleService.printSchedule()
    }
}

class UpdateSessionService(
    private var scheduleService: ScheduleService,
    private var sessionController: SessionController
) : InputService(

) {


    private fun updateComponent(session: Session) {
        println("Do you want to update start time of session?(y/n)")
        val answer: String = readln()
        if (answer == "y") {
            sessionController.updateStartTime(session)
        }
    }


    override fun startOption() {
        val session: Session = sessionController.getSession()
        println(session)
        updateComponent(session)
        scheduleService.save()
    }
}

class DeleteSessionService(
    private var scheduleService: ScheduleService,
    private var sessionController: SessionController
) : InputService() {

    override fun startOption() {
        val session: Session = sessionController.getSession()
        scheduleService.deleteSession(session)
        println("Successfully deleted")
    }
}

class BuyTicketService(
    private var scheduleService: ScheduleService,
    private var sessionController: SessionController,
    private var ticketController: TicketController,
) : InputService() {
    override fun startOption() {
        val session: Session = sessionController.getSession()
        val ticket: Ticket = ticketController.getTicket(session)
        scheduleService.takeSeat(ticket.seat.row, ticket.seat.number, session)
        scheduleService.addTicket(ticket, session)
        println("Successfully buy ticket")
    }
}

class RefundTicketService(
    private var scheduleService: ScheduleService,
    private var sessionController: SessionController,
    private var ticketController: TicketController,
) : InputService() {
    override fun startOption() {
        val session: Session = sessionController.getSession()
        if (java.time.LocalDateTime.now() >= session.startTimeFilm.toJavaLocalDateTime()) {
            println("Cannot refund ticket.Session has already started")
            return
        }
        val ticketRow = ticketController.getSeatToRefund(session)
        val ticket: Ticket? =
            session.tickets.find { it.seat.row == ticketRow.first && it.seat.number == ticketRow.second }
        if (ticket == null) {
            println("Cannot refund ticket.Ticket does not belong to session")
            return
        }
        scheduleService.returnSeat(ticket.seat.row, ticket.seat.number, session)
        scheduleService.removeTicket(ticket, session)
        println("Successfully refund ticket")
    }
}

class ViewAuditoriumService(
    private val sessionController: SessionController,
) : InputService() {
    override fun startOption() {
        val session: Session = sessionController.getSession()
        print(session.auditorium)
    }
}

class TakeSeatService(
    private var scheduleService: ScheduleService,
    private var sessionController: SessionController,
    private var ticketController: TicketController,
) : InputService() {
    override fun startOption() {
        val session: Session = sessionController.getSession()
        val seatRowNumber = ticketController.getSeat(session)
        scheduleService.takeSeat(seatRowNumber.first, seatRowNumber.second, session)
        scheduleService.save()
    }
}