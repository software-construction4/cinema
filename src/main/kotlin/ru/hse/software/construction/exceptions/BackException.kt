package ru.hse.software.construction.exceptions

class BackException(
    message: String
) : Exception(message)