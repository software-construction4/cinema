package ru.hse.software.construction.models

import kotlinx.serialization.Serializable
import kotlin.time.Duration

@Serializable
class Film(
    var name: String,
    var description: String,
    var duration: Duration
) {
    override fun toString(): String {
        return "Film\nName of film: ${name}\ndescription: ${description}\nduration: ${duration}\n"
    }
}