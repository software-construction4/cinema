package ru.hse.software.construction.models

import kotlinx.serialization.Serializable

@Serializable
class Seat(
    var isEmpty: Boolean = true,
    var row: Int,
    var number: Int,

    ) {

}